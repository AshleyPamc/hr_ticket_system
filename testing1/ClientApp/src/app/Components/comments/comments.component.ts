import { Component, OnInit, Input } from '@angular/core';
import { AppService } from '../../services/app.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Comments } from '../../models/comments';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {

  @Input() commentForm: FormGroup;
  public _openModel: boolean = false;
  public _edit: boolean = false;
  public _id: string;

  constructor(public _appService: AppService, private fb: FormBuilder) {
    this._appService.AdminService.GetComments();
  }

  ngOnInit() {

    this.commentForm = this.fb.group({
      'desc': ['', Validators.required]
    });
  }

  EditComment(c: Comments) {
    this._id = c.id;
    this.commentForm.reset();
    this._edit = true;
    this.commentForm.controls['desc'].setValue(c.comment);
    this._openModel = true;
  }

  CreateNew() {
    this._edit = false;
    this.commentForm.reset();
    this._openModel = true;
  }

  AddNewComment() {
    let c: Comments = new Comments();
    c.comment = this.commentForm.get('desc').value;
    this._appService.AdminService.AddComment(c);
    this.commentForm.reset();
    this._edit = false;
    this._openModel = false;
  }

  UpdateComment() {
    let c: Comments = new Comments();
    c.comment = this.commentForm.get('desc').value;
    c.id = this._id;
    this._appService.AdminService.UpdateComment(c);
    this.commentForm.reset();
    this._edit = false;
    this._openModel = false;
  }

}
