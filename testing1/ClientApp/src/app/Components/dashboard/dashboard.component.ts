import { Component, OnInit, Input ,OnDestroy} from '@angular/core';
import { AppService } from '../../services/app.service';
import { FormGroup, FormBuilder, Validators, ValidationErrors } from '@angular/forms';
import { ToastrService } from 'ngx-toastr'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, OnDestroy {


    @Input() newForm: FormGroup;
    private _errMsg: ValidationErrors;
    public newPassErr: string = '';
    public confPassErr: string = '';
  public time = new Date();
  public timer;


    constructor(public _appService: AppService, private _fbNew: FormBuilder, private _toastService: ToastrService) {
        this._errMsg = {
            invalid: 'The password provided is incorrect!',
            required: 'This field is required!',
            format: 'Your password must be at least 8 charachters long!',
            match: 'your passwords do not match!'
        }

        //this._toastService.info('TESTING TOAST NOTIFICATIONS', 'TESTING', { positionClass: 'toast-bottom-right' });
    }

    ngOnInit() {
      this.newForm = this._fbNew.group({
        'oldPassword': ['', Validators.required],
        'newPassword': ['', Validators.required],
        'confirmPassword': ['', Validators.required]
      });

      this.timer = setInterval(() => {
        this.time = new Date();
      }, 1000);
    }

  ngOnDestroy() {
    clearInterval(this.timer);
  }

    CheckPassword() {
        this._appService.LoginService.oldPasswordError = '';
        let c = this.newForm.get('oldPassword').value;
        if ((c == undefined) || (c == null) || (c == '')) {
            this._appService.LoginService.oldPasswordError = this._errMsg['required'];
            let msg = this._errMsg['required']
            this.newForm.get('oldPassword').setErrors(msg);
        } else {
            this._appService.LoginService.changePassword.oldPassword = this.newForm.get('oldPassword').value;
            this._appService.LoginService.changePassword.username = this._appService.LoginService.logindetails.username;
            this._appService.LoginService.ConfirmPasswordOld();
            while (!this._appService.LoginService._busy) {
                console.log('');
            }
            if (this._appService.LoginService.oldPasswordError !== '') {
                let msg = this._errMsg['invalid'];
                this.newForm.get('oldPassword').setErrors(msg);
            }
        }

    }

    LengthCheck() {
        let c = this.newForm.get('newPassword').value;
        let d = this.newForm.get('confirmPassword').value;
        let x = this.newForm.get('newPassword').touched;
        let y = this.newForm.get('confirmPassword').touched;

        this.newPassErr = '';
        this.confPassErr = '';

        if (x) {
            if ((c == undefined) || (c == null) || (c == '')) {
                let msg = this._errMsg['invalid'];
                this.newPassErr = this._errMsg['invalid'];
                this.newForm.get('newPassword').setErrors(msg);
            }
            if (c.length < 8) {
                let msg = this._errMsg['format'];
                this.newPassErr = this._errMsg['format'];
                this.newForm.get('newPassword').setErrors(msg);
            }
        }
        if (y) {
            if ((d == undefined) || (d == null) || (d == '')) {
                let msg = this._errMsg['invalid'];
                this.confPassErr = this._errMsg['invalid'];
                this.newForm.get('confirmPassword').setErrors(msg);
            }
            if (d.length < 8) {
                let msg = this._errMsg['format'];
                this.confPassErr = this._errMsg['format'];
                this.newForm.get('confirmPassword').setErrors(msg);
            }

        }


        if ((y && x)) {
            this.ConfirmMatch();
        }


        
    }

    ConfirmMatch() {

        let c = this.newForm.get('newPassword').value;
        let d = this.newForm.get('confirmPassword').value;

        if (c !== d) {
            let msg = this._errMsg['match'];
            this.newPassErr = this._errMsg['match'];
            this.newForm.get('newPassword').setErrors(msg);
            this.confPassErr = this._errMsg['match'];
            this.newForm.get('confirmPassword').setErrors(msg);
        } else {
            this.newPassErr = '';
            this.confPassErr = '';
            this.newForm.get('confirmPassword').setErrors(null);
            this.newForm.get('newPassword').setErrors(null);
        }

    }

    ChangePassword() {
        this._appService.LoginService.changePassword.newPassword = this.newForm.get('newPassword').value;
        this._appService.LoginService.ChangePassword();
    }
}
