import { Component, OnInit } from '@angular/core';
import { AppService } from '../../services/app.service';

@Component({
  selector: 'app-importanc',
  templateUrl: './importanc.component.html',
  styleUrls: ['./importanc.component.css']
})
export class ImportancComponent implements OnInit {

  constructor(public _appService: AppService) {
    this._appService.AdminService.GetLevelOfImportance();
    
  }

  ngOnInit() {
  }

}
