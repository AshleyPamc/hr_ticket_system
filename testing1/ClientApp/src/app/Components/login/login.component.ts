import { Component, OnInit, Input } from '@angular/core';
import { AppService } from '../../services/app.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Login } from '../../models/login';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    @Input() LoginForm: FormGroup;

    constructor(public _appService: AppService, private _fb: FormBuilder) {

        this._appService.LoginService.TestConnection();
    }

    ngOnInit() {

        this.LoginForm = this._fb.group({
            'username': ['', Validators.required],
            'password': ['', Validators.required]
        }) 
  }

    LogIn() {
        console.log('checkpoint');
        this._appService.LoginService.logindetails = new Login();
        this._appService.LoginService.logindetails.username = this.LoginForm.get('username').value;
        this._appService.LoginService.logindetails.password = this.LoginForm.get('password').value;
        this._appService.LoginService.ConfirmLogin();


    }
}
