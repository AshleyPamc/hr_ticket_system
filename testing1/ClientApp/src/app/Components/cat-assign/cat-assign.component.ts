import { Component, OnInit, Input } from '@angular/core';
import { AppService } from '../../services/app.service';
import { Categories } from '../../models/categories';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Users } from '../../models/users';

@Component({
  selector: 'app-cat-assign',
  templateUrl: './cat-assign.component.html',
  styleUrls: ['./cat-assign.component.css']
})
export class CatAssignComponent implements OnInit {


  public _openModel = false;
  public _edit: boolean = false;
  public _selectedRec: Categories = new Categories();

  @Input() catAsForm: FormGroup;

  constructor(public _appService: AppService, private fb: FormBuilder) {
    this._appService.AdminService.GetCatAssign();
    this._appService.AdminService.GetCategories();
    this._appService.AdminService.GetUsers();
    this._appService.AdminService.GetAdminUsers();
    this._appService.AdminService.GetClients();
    console.log(this._appService.AdminService.users);
  }

  ngOnInit() {
    this.catAsForm = this.fb.group({
      'client': ['', Validators.required],
      'cat': ['', Validators.required],
      'assign': ['', Validators.required]
    })
  }


  OpenEdit(data: Categories) {
    this._selectedRec = new Categories();
    this._selectedRec = data;
    this.catAsForm.reset();
    this.catAsForm.controls['assign'].setValue(this._selectedRec.username);
    this._edit = true;
    this._openModel = true;


  }

  CreateNew() {
    let tem: Users[] = [];
    this._appService.AdminService.users.forEach((user) => {
      if ((user.id == '15') || (user.id == '13') || (user.id == '11')) {
        tem.push(user);
      }
    });
    this._appService.AdminService.users = [];
    this._appService.AdminService.users = tem;
    this.catAsForm.reset();
    this._edit = false;
    this._openModel = true;
    
  }

  AddNewCategoryAssign() {
    let c: Categories = new Categories();

    c.clientId = this.catAsForm.get('client').value;
    c.id = this.catAsForm.get('cat').value;
    c.username = this.catAsForm.get('assign').value;
    this._appService.AdminService.AddCatAssign(c);
    this._edit = false;
    this.catAsForm.reset();
    this._openModel = false;
  }

  UpdateCategoryAssign() {
    let c: Categories = new Categories();

    c.clientId = this._selectedRec.clientId;
    c.id = this._selectedRec.id;
    c.username = this.catAsForm.get('assign').value;

    this._edit = false;
    this.catAsForm.reset();
    this._openModel = false;

    this._appService.AdminService.UpdateCatAssign(c);
  }

  Test() {
    console.log(this.catAsForm.get('cat').valid);
    console.log(this.catAsForm.get('cat').value);
  }
}
