import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Importance } from '../models/importance';
import { Status } from '../models/status';
import { Clients } from '../models/clients';
import { ValidationResultsModel } from '../models/ValidationResultsModel';
import { Users } from '../models/users';
import { Comments } from '../models/comments';
import { Categories } from '../models/categories';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  private _importanceList: Importance[] = [];
  private _status: Status[] = [];
  private _clients: Clients[] = [];
  private _users: Users[] = [];
  private _adminUsers: Users[] = [];
  private _comments: Comments[] = [];
  private _categories: Categories[] = [];
  private _cateas: Categories[] = [];
  public _busy: boolean = false;
  public _error: boolean = false;

  constructor(private _http: HttpClient, @Inject('BASE_URL') private _baseUrl: string, public _router: Router) {

  }

  public get importanceList(): Importance[] {
    return this._importanceList;
  }
  public set importanceList(value: Importance[]) {
    this._importanceList = value;
  }

  public get status(): Status[] {
    return this._status;
  }
  public set status(value: Status[]) {
    this._status = value;
  }

  public get clients(): Clients[] {
    return this._clients;
  }
  public set clients(value: Clients[]) {
    this._clients = value;
  }

  public get users(): Users[] {
    return this._users;
  }
  public set users(value: Users[]) {
    this._users = value;
  }

  public get adminUsers(): Users[] {
    return this._adminUsers;
  }
  public set adminUsers(value: Users[]) {
    this._adminUsers = value;
  }
  public get comments(): Comments[] {
    return this._comments;
  }
  public set comments(value: Comments[]) {
    this._comments = value;
  }

  public get categories(): Categories[] {
    return this._categories;
  }
  public set categories(value: Categories[]) {
    this._categories = value;
  }

  public get cateas(): Categories[] {
    return this._cateas;
  }
  public set cateas(value: Categories[]) {
    this._cateas = value;
  }

  GetLevelOfImportance() {
    this._busy = true;
    //let token = localStorage.getItem('token');
    this._http.get(this._baseUrl + 'api/Admin/GetImportance',
      {
        headers: new HttpHeaders({
          //"Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((results: Importance[]) => {
        this._importanceList = [];
        results.forEach((el) => {
          this._importanceList.push(el);
        });
        
      },
        (error) => {
          console.log(error);
          this._error = true;
          this._busy = false;
        },
        () => {
          this._busy = false;
        })

  }

  GetStatus() {
    this._busy = true;
    //let token = localStorage.getItem('token');
    this._http.get(this._baseUrl + 'api/Admin/GetStatus',
      {
        headers: new HttpHeaders({
          //"Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((results: Status[]) => {
        this._status = [];
        results.forEach((el) => {
          this._status.push(el);
        });

      },
        (error) => {
          console.log(error);
          this._error = true;
          this._busy = false;
        },
        () => {
          this._busy = false;
        })

  }

  GetComments() {
    this._busy = true;
    //let token = localStorage.getItem('token');
    this._http.get(this._baseUrl + 'api/Admin/GetComments',
      {
        headers: new HttpHeaders({
          //"Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((results: Comments[]) => {
        this._comments = [];
        results.forEach((el) => {
          this._comments.push(el);
        });

      },
        (error) => {
          console.log(error);
          this._error = true;
          this._busy = false;
        },
        () => {
          this._busy = false;
        })

  }

  GetClients() {
    this._busy = true;
    //let token = localStorage.getItem('token');
    this._http.get(this._baseUrl + 'api/Admin/GetClients',
      {
        headers: new HttpHeaders({
          //"Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((results: Clients[]) => {
        this.clients = [];
        results.forEach((el) => {
          this.clients.push(el);
        });

      },
        (error) => {
          console.log(error);
          this._error = true;
          this._busy = false;
        },
        () => {
          this._busy = false;
        })

  }

  GetUsers() {
    this._busy = true;
    //let token = localStorage.getItem('token');
    this._http.get(this._baseUrl + 'api/Admin/GetUsers',
      {
        headers: new HttpHeaders({
          //"Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((results: Users[]) => {
        this.users = [];
        results.forEach((el) => {
          this.users.push(el);
        });

      },
        (error) => {
          console.log(error);
          this._error = true;
          this._busy = false;
        },
        () => {
          this._busy = false;
        })

  }

  GetAdminUsers() {
    this._busy = true;
    //let token = localStorage.getItem('token');
    this._http.get(this._baseUrl + 'api/Admin/GetAdminUsers',
      {
        headers: new HttpHeaders({
          //"Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((results: Users[]) => {
        this.adminUsers = [];
        results.forEach((el) => {
          this.adminUsers.push(el);
        });

      },
        (error) => {
          console.log(error);
          this._error = true;
          this._busy = false;
        },
        () => {
          this._busy = false;
        })

  }

  GetCatAssign() {
    this._busy = true;
    //let token = localStorage.getItem('token');
    this._http.get(this._baseUrl + 'api/Admin/GetCatAssign',
      {
        headers: new HttpHeaders({
          //"Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((results: Categories[]) => {
        this.cateas = [];
        results.forEach((el) => {
          this.cateas.push(el);
        });

      },
        (error) => {
          console.log(error);
          this._error = true;
          this._busy = false;
        },
        () => {
          this._busy = false;
        })

  }

  GetCategories() {
    this._busy = true;
    //let token = localStorage.getItem('token');
    this._http.get(this._baseUrl + 'api/Admin/GetCategories',
      {
        headers: new HttpHeaders({
          //"Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((results: Categories[]) => {
        this.categories = [];
        results.forEach((el) => {
          this.categories.push(el);
        });

      },
        (error) => {
          console.log(error);
          this._error = true;
          this._busy = false;
        },
        () => {
          this._busy = false;
        })

  }

  UpdateClient(data: Clients) {
    this._busy = true;
    //let token = localStorage.getItem('token');
    this._http.post(this._baseUrl + 'api/Admin/UpdateClient', data,
      {
        headers: new HttpHeaders({
          //"Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((results: ValidationResultsModel) => {


      },
        (error) => {
          console.log(error);
          this._error = true;
          this._busy = false;
        },
        () => {
          this._busy = false;
          this.GetClients();
        })
  }

  AddClient(data: Clients) {
    this._busy = true;
    //let token = localStorage.getItem('token');
    this._http.post(this._baseUrl + 'api/Admin/AddClient', data,
      {
        headers: new HttpHeaders({
          //"Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((results: ValidationResultsModel) => {


      },
        (error) => {
          console.log(error);
          this._error = true;
          this._busy = false;
        },
        () => {
          this._busy = false;
          this.GetClients();
        })
  }


  AddCategory(data: Categories) {
    this._busy = true;
    //let token = localStorage.getItem('token');
    this._http.post(this._baseUrl + 'api/Admin/AddCategory', data,
      {
        headers: new HttpHeaders({
          //"Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((results: ValidationResultsModel) => {


      },
        (error) => {
          console.log(error);
          this._error = true;
          this._busy = false;
        },
        () => {
          this._busy = false;
          this.GetCategories();
        })
  }

  UpdateUser(data: Users) {
    this._busy = true;
    //let token = localStorage.getItem('token');
    this._http.post(this._baseUrl + 'api/Admin/UpdateUser', data,
      {
        headers: new HttpHeaders({
          //"Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((results: ValidationResultsModel) => {


      },
        (error) => {
          console.log(error);
          this._error = true;
          this._busy = false;
        },
        () => {
          this._busy = false;
          this.GetUsers();
        })
  }

  AddUser(data: Users) {
    this._busy = true;
    //let token = localStorage.getItem('token');
    this._http.post(this._baseUrl + 'api/Admin/AddUser', data,
      {
        headers: new HttpHeaders({
          //"Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((results: ValidationResultsModel) => {


      },
        (error) => {
          console.log(error);
          this._error = true;
          this._busy = false;
        },
        () => {
          this._busy = false;
          this.GetUsers();
        })
  }

  UpdateComment(data: Comments) {
    this._busy = true;
    //let token = localStorage.getItem('token');
    this._http.post(this._baseUrl + 'api/Admin/UpdateComment', data,
      {
        headers: new HttpHeaders({
          //"Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((results: ValidationResultsModel) => {


      },
        (error) => {
          console.log(error);
          this._error = true;
          this._busy = false;
        },
        () => {
          this._busy = false;
          this.GetComments();
        })
  }

  AddComment(data: Comments) {
    this._busy = true;
    //let token = localStorage.getItem('token');
    this._http.post(this._baseUrl + 'api/Admin/AddComment', data,
      {
        headers: new HttpHeaders({
          //"Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((results: ValidationResultsModel) => {


      },
        (error) => {
          console.log(error);
          this._error = true;
          this._busy = false;
        },
        () => {
          this._busy = false;
          this.GetComments();
        })
  }

  UpdateCatAssign(data: Categories) {
    this._busy = true;
    //let token = localStorage.getItem('token');
    this._http.post(this._baseUrl + 'api/Admin/UpdateCatAssign', data,
      {
        headers: new HttpHeaders({
          //"Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((results: ValidationResultsModel) => {


      },
        (error) => {
          console.log(error);
          this._error = true;
          this._busy = false;
        },
        () => {
          this._busy = false;
          this.GetComments();
        })
  }

  AddCatAssign(data: Categories) {
    this._busy = true;
    //let token = localStorage.getItem('token');
    this._http.post(this._baseUrl + 'api/Admin/AddCatAssign', data,
      {
        headers: new HttpHeaders({
          //"Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((results: ValidationResultsModel) => {


      },
        (error) => {
          console.log(error);
          this._error = true;
          this._busy = false;
        },
        () => {
          this._busy = false;
          this.GetCatAssign();
        })
  }

}
