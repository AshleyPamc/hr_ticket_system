import { Injectable, Inject } from '@angular/core';
import { Login } from '../models/login';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Connection } from '../models/connection';
import { Password } from '../models/password';
import { ToastrService } from 'ngx-toastr'
import { Tickets } from '../models/tickets';
import { Qoutes } from '../models/qoutes';

@Injectable({
    providedIn: 'root'
})
export class LoginService {

    private _logindetails: Login = new Login();
    public usernameError: boolean = false;
    public connectionError: boolean = false;
    public _busy: boolean = false;
    private _passwordChange: boolean;
    private _oldPasswordError: string;
    private _changePassword: Password = new Password();
  private _qouteList: Qoutes[] = [];
  private _selectedQoute: Qoutes = new Qoutes();



    constructor(private _http: HttpClient, @Inject('BASE_URL') private _baseUrl: string, public _router: Router,private _toastService: ToastrService) { }

  public get qouteList(): Qoutes[] {
    return this._qouteList;
  }
  public set qouteList(value: Qoutes[]) {
    this._qouteList = value;
  }
  public get selectedQoute(): Qoutes {
    return this._selectedQoute;
  }
  public set selectedQoute(value: Qoutes) {
    this._selectedQoute = value;
  }
    public get logindetails(): Login {
        return this._logindetails;
    }
    public set logindetails(value: Login) {
        this._logindetails = value;
    }
    public get passwordChange(): boolean {
        return this._passwordChange;
    }
    public set passwordChange(value: boolean) {
        this._passwordChange = value;
    }
    public get oldPasswordError(): string {
        return this._oldPasswordError;
    }
    public set oldPasswordError(value: string) {
        this._oldPasswordError = value;
    }
    public get changePassword(): Password {
        return this._changePassword;
    }
    public set changePassword(value: Password) {
        this._changePassword = value;
    }

    TestConnection() {
        this._busy = true;
        this._http.get(this._baseUrl + 'api/Login/TestConnection'
        ).subscribe((results: Connection) => {
            if (results.valid) {
                this.connectionError = false;
            } else {
                this.connectionError = true;
            }
        },
            (error) => {
                this.connectionError = true;
                console.log(error);
            },
            () => {
                this._busy = false;
            })
    }

    ConfirmLogin() {
        this._busy = true;
        this.usernameError = false;
        this._http.post(this._baseUrl + 'api/Login/VerifyLoginDetail', this._logindetails
        ).subscribe((results: Login) => {
            this._logindetails.name = results.name;
            this._logindetails.surname = results.surname;
            this._logindetails.type = results.type;
            this._logindetails.loggedIn = results.loggedIn;
            this._logindetails.id = results.id;
          this._logindetails.email = results.email;
          console.log(this._logindetails);
            
        },
            (error) => {

            },
            () => {
                if (this._logindetails.loggedIn) {
                  this.GetQoutes();
                  this._router.navigateByUrl('/DashBoard');
                } else {
                    this.usernameError = true;
                }

              this._busy = false;

            });
    };

    //GetToken() {
    //    this._busy = true;
    //    this._http.post(this._baseUrl + 'api/Authorization/token', this._logindetails
    //        , { responseType: "text" }).subscribe((results) => {
    //            let token = results;
    //            localStorage.setItem('token', token);
    //    },
    //            (error) => {
    //                console.log(error);
    //        },
    //            () => {
    //                
    //              this._busy = false;
    //             
    //            });
    //}

    ChangePassword() {
        this.passwordChange = false;
        this._busy = true;
        //let token = localStorage.getItem('token');
        this._http.post(this._baseUrl + 'api/login/UpdatePassword', this.changePassword, {
            headers: new HttpHeaders({
                //"Authorization": "Bearer " + token,
                "Content-Type": "application/json",
                "Cache-Control": "no-store",
                "Pragma": "no-cache"

            })
        }
        ).subscribe((results: Connection) => {
            if (results.valid) {
                this._toastService.success('Your password has been successfully updated.', 'PASSWORD UPDATE SUCCESS', { positionClass: 'toast-bottom-right' });
            } else {
                this._toastService.error('An error occured while trying to update your password.', 'PASSWORD UPDATE FAILED', { positionClass: 'toast-bottom-right' });
            }
        },
            (error) => { console.log(error) },
            () => {
                this._busy = false;
               
            })

    }

    ConfirmPasswordOld() {
        this._busy = true;
        //let token = localStorage.getItem('token');
        this._http.post(this._baseUrl + 'api/Login/ConfirmPasswordOld', this.changePassword,
            {
                headers: new HttpHeaders({
                    //"Authorization": "Bearer " + token,
                    "Content-Type": "application/json",
                    "Cache-Control": "no-store",
                    "Pragma": "no-cache"

                })
            }
        ).subscribe((results: Connection) => {
            if (results.valid) {
                this._oldPasswordError = '';
            } else {
                this._oldPasswordError = 'The password provided is incorrect!';
            }
        },
            (error) => { console.log(error) },
            () => {
                this._busy = false;
            })
    }

  GetQoutes() {
    this._busy = true;
    //let token = localStorage.getItem('token');
    this._http.get(this._baseUrl + 'api/Ticket/GetQoutes', {
      headers: new HttpHeaders({
        //"Authorization": "Bearer " + token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }).subscribe((results: Qoutes[]) => {
      this._qouteList = [];
      results.forEach((el) => {
        this._qouteList.push(el);
      });

      let count: number = Math.floor(Math.random() * (43 - 1 + 1) + 1);
      this.selectedQoute = this._qouteList[count];


    },
      (error) => { console.log(error) },
      () => { this._busy = false; });
  }
}
