export class Login {
    public id: number;
    public name: string;
    public surname: string;
    public username: string;
    public password: string;
    public email: string;
    public type: string;
    public loggedIn: boolean;
}
