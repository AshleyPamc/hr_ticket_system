export class Users {
  public  client:string;
  public  name:string;
  public  surname:string;
  public username: string;
  public password: string;
  public  email:string;
  public  createDate:string;
  public  changeDate:string;
  public type: string;
  public id: string;
}
