﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using pamcportal.Models;
using testing1.models;

namespace testing1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : Base.Contex
    {
        public AdminController(IHostingEnvironment env, IConfiguration con) : base(env, con)
        {

        }

        [HttpGet]
        [Route("GetClients")]
        //[Authorize(Roles = "user")]
        public List<Clients> GetClients()
        {
            List<Clients> list = new List<Clients>();
            try
            {

                using (SqlConnection cn = new SqlConnection(_ticketConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    DataTable dt = new DataTable();

                    cmd.CommandText = $"SELECT * FROM {TicketDatabase}.dbo.Clients";

                    dt.Load(cmd.ExecuteReader());

                    foreach (DataRow row in dt.Rows)
                    {
                        Clients c = new Clients();

                        c.id = row["clientId"].ToString();
                        c.name = row["clientName"].ToString();
                        c.accronym = row["acronym"].ToString();
                        c.threshold = row["acknowledgeThreshold"].ToString();

                        list.Add(c);

                    }
    
            }

                return list;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                throw;
            }
        }

        [HttpGet]
        [Route("GetUsers")]
        //[Authorize(Roles = "user")]
        public List<Users> GetUsers()
        {
            List<Users> list = new List<Users>();
            try
            {

                using (SqlConnection cn = new SqlConnection(_ticketConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    DataTable dt = new DataTable();
                    string sql = "SELECT \n"
           + "      u.Name\n"
           + "      , u.Surname\n"
           + "      , u.Username\n"
           + "      , u.Email\n"
           + "      , u.CreateDate\n"
           + "      , u.ChangeDate\n"
           + "      , u.Password\n"
           + "	  , c.clientName\n"
                      + "	  , u.clientId\n"
           + "	  , t.UserType\n"
           + $"  FROM {TicketDatabase}.dbo.Users u\n"
           + $"  INNER JOIN {TicketDatabase}.dbo.Clients c ON c.clientId = u.clientId\n"
           + $"  INNER JOIN {TicketDatabase}.dbo.UserType t ON t.userId = u.UserType";

                    cmd.CommandText =sql;

                    dt.Load(cmd.ExecuteReader());

                    foreach (DataRow row in dt.Rows)
                    {
                        Users c = new Users();

                        c.name = row["Name"].ToString();
                        c.surname = row["Surname"].ToString();
                        c.username = row["Username"].ToString();
                        c.email = row["Email"].ToString();
                        c.id = row["clientId"].ToString();
                        c.createDate = row["CreateDate"].ToString();
                        c.changeDate = row["ChangeDate"].ToString();
                        c.client = row["clientName"].ToString();
                        c.type = row["UserType"].ToString().Trim();
                        c.password = row["Password"].ToString();
                        list.Add(c);

                    }

                }

                return list;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                throw;
            }
        }

        [HttpGet]
        [Route("GetAdminUsers")]
        //[Authorize(Roles = "user")]
        public List<Users> GetAdminUsers()
        {
            List<Users> list = new List<Users>();
            try
            {

                using (SqlConnection cn = new SqlConnection(_ticketConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    DataTable dt = new DataTable();
                    string sql = "SELECT \n"
           + "      u.Name\n"
           + "      , u.Surname\n"
           + "      , u.Username\n"
           + "      , u.Email\n"
           + "      , u.CreateDate\n"
           + "      , u.ChangeDate\n"
           + "      , u.Password\n"
           + "	  , c.clientName\n"
                      + "	  , u.clientId\n"
           + "	  , t.UserType\n"
           + $"  FROM {TicketDatabase}.dbo.Users u\n"
           + $"  INNER JOIN {TicketDatabase}.dbo.Clients c ON c.clientId = u.clientId\n"
           + $"  INNER JOIN {TicketDatabase}.dbo.UserType t ON t.userId = u.UserType" +
           $" WHERE t.UserType = 'SysAdmin'";

                    cmd.CommandText = sql;

                    dt.Load(cmd.ExecuteReader());

                    foreach (DataRow row in dt.Rows)
                    {
                        Users c = new Users();

                        c.name = row["Name"].ToString();
                        c.surname = row["Surname"].ToString();
                        c.username = row["Username"].ToString();
                        c.email = row["Email"].ToString();
                        c.id = row["clientId"].ToString();
                        c.createDate = row["CreateDate"].ToString();
                        c.changeDate = row["ChangeDate"].ToString();
                        c.client = row["clientName"].ToString();
                        c.type = row["UserType"].ToString().Trim();
                        c.password = row["Password"].ToString();
                        list.Add(c);

                    }

                }

                return list;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                throw;
            }
        }

        [HttpGet]
        [Route("GetCategories")]
        //[Authorize(Roles = "user")]
        public List<Categories> GetCategories()
        {
            List<Categories> list = new List<Categories>();
            try
            {

                using (SqlConnection cn = new SqlConnection(_ticketConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    DataTable dt = new DataTable();


                    cmd.CommandText = $"SELECT * FROM {TicketDatabase}.dbo.TicketCategory";

                    dt.Load(cmd.ExecuteReader());

                    foreach (DataRow row in dt.Rows)
                    {
                        Categories c = new Categories();

                        c.desc = row["categoryDescription"].ToString();
                        c.id = row["categoryId"].ToString();
                        c.sla = Convert.ToBoolean(row["categorySLA"].ToString());

                        list.Add(c);

                    }

                }

                return list;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                throw;
            }
        }

        [HttpGet]
        [Route("GetCatAssign")]
        //[Authorize(Roles = "user")]
        public List<Categories> GetCatAssign()
        {
            List<Categories> list = new List<Categories>();
            try
            {

                using (SqlConnection cn = new SqlConnection(_ticketConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    DataTable dt = new DataTable();

                    string sql = "SELECT  c.clientName,c.clientId,t.categoryId\n"
           + "      ,t.categoryDescription\n"
           + "      ,cc.userName\n"
           + $"  FROM {TicketDatabase}.dbo.ClientCategory cc\n"
           + $"  INNER JOIN {TicketDatabase}.dbo.Clients c ON c.clientId = cc.clientId\n"
           + $"  INNER JOIN {TicketDatabase}.dbo.TicketCategory t ON t.categoryId = cc.categoryId";

                    cmd.CommandText = sql;

                    dt.Load(cmd.ExecuteReader());

                    foreach (DataRow row in dt.Rows)
                    {
                        Categories c = new Categories();

                        c.id = row["categoryId"].ToString();
                        c.desc = row["categoryDescription"].ToString();
                        c.username = row["userName"].ToString();
                        c.client = row["clientName"].ToString();
                        c.clientId = row["clientId"].ToString();

                        list.Add(c);

                    }

                }

                return list;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                throw;
            }
        }


        [HttpGet]
        [Route("GetImportance")]
        //[Authorize(Roles = "user")]
        public List<Importance> GetImportance()
        {
            List<Importance> list = new List<Importance>();
            try
            {

                using (SqlConnection cn = new SqlConnection(_ticketConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    DataTable dt = new DataTable();

                    cmd.CommandText = $"SELECT * FROM {TicketDatabase}.dbo.LevelOfImportance";

                    dt.Load(cmd.ExecuteReader());

                    foreach (DataRow row in dt.Rows)
                    {
                        Importance c = new Importance();

                        c.id = row["importanceCode"].ToString();
                        c.desc = row["importanceDescription"].ToString();

                        list.Add(c);

                    }

                }

                return list;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                throw;
            }
        }


        [HttpGet]
        [Route("GetStatus")]
        //[Authorize(Roles = "user")]
        public List<Status> GetStatus()
        {
            List<Status> list = new List<Status>();
            try
            {

                using (SqlConnection cn = new SqlConnection(_ticketConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    DataTable dt = new DataTable();

                    cmd.CommandText = $"SELECT * FROM {TicketDatabase}.dbo.TicketStatus";

                    dt.Load(cmd.ExecuteReader());

                    foreach (DataRow row in dt.Rows)
                    {
                        Status c = new Status();

                        c.code = row["StatusCode"].ToString();
                        c.desc = row["StatusDescription"].ToString();

                        list.Add(c);

                    }

                }

                return list;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                throw;
            }
        }


        [HttpGet]
        [Route("GetComments")]
        //[Authorize(Roles = "user")]
        public List<Comments> GetComments()
        {
            List<Comments> list = new List<Comments>();
            try
            {

                using (SqlConnection cn = new SqlConnection(_ticketConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    DataTable dt = new DataTable();

                    cmd.CommandText = $"SELECT * FROM {TicketDatabase}.dbo.AdminComment";

                    dt.Load(cmd.ExecuteReader());

                    foreach (DataRow row in dt.Rows)
                    {
                        Comments c = new Comments();

                        c.id = row["commentId"].ToString();
                        c.comment = row["commentDescription"].ToString();

                        list.Add(c);

                    }

                }

                return list;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                throw;
            }
        }
   
        [HttpPost]
        [Route("UpdateClient")]
        //[Authorize(Roles = "user")]
        public ValidationResultsModel UpdateClients([FromBody] Clients data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            try
            {
                using(SqlConnection cn = new SqlConnection(_ticketConnectionString))
                {
                    if(cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    cmd.Parameters.Add(new SqlParameter("@id", data.id));
                    cmd.Parameters.Add(new SqlParameter("@name", data.name));
                    cmd.Parameters.Add(new SqlParameter("@thresh", data.threshold));
                    cmd.Parameters.Add(new SqlParameter("@acc", data.accronym));

                    cmd.CommandText = $"UPDATE {TicketDatabase}.dbo.Clients SET clientName = @name, acronym = @acc, acknowledgeThreshold = @thresh WHERE clientId = @id";

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {

                throw;
            }
            return vr;
        }

        [HttpPost]
        [Route("AddClient")]
        //[Authorize(Roles = "user")]
        public ValidationResultsModel AddClient([FromBody] Clients data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            try
            {
                using (SqlConnection cn = new SqlConnection(_ticketConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    cmd.Parameters.Add(new SqlParameter("@name", data.name));
                    cmd.Parameters.Add(new SqlParameter("@thresh", data.threshold));
                    cmd.Parameters.Add(new SqlParameter("@acc", data.accronym));

                    cmd.CommandText = $"INSERT INTO {TicketDatabase}.dbo.Clients (clientName,acronym,acknowledgeThreshold) \n" +
                        $"VALUES(@name,@acc,@thresh)";

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {

                throw;
            }
            return vr;
        }

        [HttpPost]
        [Route("UpdateUser")]
        //[Authorize(Roles = "user")]
        public ValidationResultsModel UpdateUser([FromBody] Users data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            try
            {
                using (SqlConnection cn = new SqlConnection(_ticketConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

   
                    cmd.Parameters.Add(new SqlParameter("@name", data.name));
                    cmd.Parameters.Add(new SqlParameter("@surname", data.surname));
                    cmd.Parameters.Add(new SqlParameter("@client", data.client));
                    cmd.Parameters.Add(new SqlParameter("@email", data.email));
                    cmd.Parameters.Add(new SqlParameter("@username", data.username));
                    cmd.Parameters.Add(new SqlParameter("@password", data.password));
                    cmd.Parameters.Add(new SqlParameter("@type", data.type));
                    cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T"," ")));
                    cmd.CommandText = $"UPDATE {TicketDatabase}.dbo.Users SET Name = @name, Surname = @surname, clientId = @client , Email= @email,Password = @password, UserType=@type, ChangeDate=@date WHERE Username = @username";

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {

                throw;
            }
            return vr;
        }

        [HttpPost]
        [Route("AddUser")]
        //[Authorize(Roles = "user")]
        public ValidationResultsModel AddUser([FromBody] Users data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            try
            {
                using (SqlConnection cn = new SqlConnection(_ticketConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                    cmd.Parameters.Add(new SqlParameter("@name", data.name));
                    cmd.Parameters.Add(new SqlParameter("@surname", data.surname));
                    cmd.Parameters.Add(new SqlParameter("@client", data.client));
                    cmd.Parameters.Add(new SqlParameter("@email", data.email));
                    cmd.Parameters.Add(new SqlParameter("@username", data.username));
                    cmd.Parameters.Add(new SqlParameter("@password", data.password));
                    cmd.Parameters.Add(new SqlParameter("@type", data.type));
                    cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")));
                    cmd.CommandText = $"INSERT INTO {TicketDatabase}.dbo.Users  (Name,Username,Surname,clientId,Email,Password,UserType,ChangeDate,CreateDate) \n " +
                        $"VALUES (@name,@username,@surname, @client,@email,@password,@type,@date,@date)";


                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {

                throw;
            }
            return vr;
        }


        [HttpPost]
        [Route("UpdateComment")]
        //[Authorize(Roles = "user")]
        public ValidationResultsModel UpdateComment([FromBody] Comments data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            try
            {
                using (SqlConnection cn = new SqlConnection(_ticketConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    cmd.Parameters.Add(new SqlParameter("@id", data.id));
                    cmd.Parameters.Add(new SqlParameter("@desc", data.comment));

                    cmd.CommandText = $"UPDATE {TicketDatabase}.dbo.AdminComment SET commentDescription = @desc WHERE commentId = @id";

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {

                throw;
            }
            return vr;
        }

        [HttpPost]
        [Route("AddComment")]
        //[Authorize(Roles = "user")]
        public ValidationResultsModel AddComment([FromBody] Comments data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            try
            {
                using (SqlConnection cn = new SqlConnection(_ticketConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    cmd.Parameters.Add(new SqlParameter("@desc", data.comment));


                    cmd.CommandText = $"INSERT INTO {TicketDatabase}.dbo.AdminComment (commentDescription) \n" +
                        $"VALUES(@desc)";

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {

                throw;
            }
            return vr;
        }

        [HttpPost]
        [Route("AddCategory")]
        //[Authorize(Roles = "user")]
        public ValidationResultsModel AddCategory([FromBody] Categories data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            try
            {
                using (SqlConnection cn = new SqlConnection(_ticketConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    cmd.Parameters.Add(new SqlParameter("@desc", data.desc));
                    cmd.Parameters.Add(new SqlParameter("@sla", data.sla));

                    cmd.CommandText = $"INSERT INTO {TicketDatabase}.dbo.TicketCategory (categoryDescription,categorySLA) \n" +
                        $"VALUES(@desc,@sla)";

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {

                throw;
            }
            return vr;
        }

        [HttpPost]
        [Route("AddCatAssign")]
        //[Authorize(Roles = "user")]
        public ValidationResultsModel AddCatAssign([FromBody] Categories data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            try
            {
                using (SqlConnection cn = new SqlConnection(_ticketConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    cmd.Parameters.Add(new SqlParameter("@desc", data.id));
                    cmd.Parameters.Add(new SqlParameter("@id", data.clientId));
                    cmd.Parameters.Add(new SqlParameter("@user", data.username));

                    cmd.CommandText = $"INSERT INTO {TicketDatabase}.dbo.ClientCategory (clientId,categoryId,userName) \n" +
                        $"VALUES(@id,@desc,@user)";

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {

                throw;
            }
            return vr;
        }

        [HttpPost]
        [Route("UpdateCatAssign")]
        //[Authorize(Roles = "user")]
        public ValidationResultsModel UpdateCatAssign([FromBody] Categories data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            try
            {
                using (SqlConnection cn = new SqlConnection(_ticketConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    cmd.Parameters.Add(new SqlParameter("@desc", data.id));
                    cmd.Parameters.Add(new SqlParameter("@id", data.clientId));
                    cmd.Parameters.Add(new SqlParameter("@user", data.username));

                    cmd.CommandText = $"UPDATE {TicketDatabase}.dbo.ClientCategory SET userName = @user WHERE clientId = @id AND  categoryId = @desc\n";
                       

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {

                throw;
            }
            return vr;
        }
    }
}
