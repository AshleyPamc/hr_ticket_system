﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.NetworkInformation;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using pamcportal.Models;
using testing1.models;
using Sugoi.Utils.WindowsNetwork;

namespace testing1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TicketController : Base.Contex
    {
        public TicketController(IHostingEnvironment env, IConfiguration con) : base(env, con)
        {

        }

        [HttpPost]
        [Route("GetCompletedTickets")]
        //[Authorize(Roles = "user")]
        public List<Tickets> GetCompletedTickets([FromBody] login data)
        {
            List<Tickets> list = new List<Tickets>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_ticketConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }
                    SqlCommand cmd = new SqlCommand();
                    DataTable dt = new DataTable();
                    string date = DateTime.Now.AddMonths(-6).ToString("yyyy/MM/ddTHH:mm:ss").Replace("T"," ");
                    cmd.Parameters.Add(new SqlParameter("@username", data.username));
                    cmd.Parameters.Add(new SqlParameter("@status", "1"));
                    cmd.Parameters.Add(new SqlParameter("@date", date));
                    cmd.Connection = cn;

                    string sql = "SELECT Ticket.CreateDate, \n"
           + "       Ticket.Username, \n"
           + "       TIcket.Email, \n"
           + "       Ticket.Subject, \n"
           + "       Ticket.Detail, \n"
           + "       Ticket.Reference, " +
           "         Ticket.CreateDate,\n"
           + "       tc.categoryDescription, \n"
           + "       LI.importanceDescription, \n"
           + "       Ticket.AcknowledgeDate, \n"
           + "       Ticket.completedTime\n," +
           "Ticket.threadId \n"
           + "FROM Ticket\n"
           + "     INNER JOIN TicketCategory tc ON tc.categoryId = Ticket.categoryId\n"
           + "     INNER JOIN LevelOfImportance LI ON LI.importanceCode = TIcket.importanceCode\n"
           + "WHERE Ticket.AssignedTo = @username AND Ticket.isCompleted = @status AND Ticket.CreateDate >= @date ORDER BY Ticket.CreateDate DESC";

                    cmd.CommandText = sql;

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }

                    foreach (DataRow row in dt.Rows)
                    {
                        Tickets item = new Tickets();

                        item.aknowledge = row["AcknowledgeDate"].ToString();
                        item.user = row["Username"].ToString();
                        item.message = row["Detail"].ToString();
                        item.subject = row["Subject"].ToString();
                        item.reference = row["Reference"].ToString();
                        item.importance = row["importanceDescription"].ToString();
                        item.category = row["categoryDescription"].ToString();
                        item.createDate = row["CreateDate"].ToString();
                        item.completedTime = row["completedTime"].ToString();
                        item.email = row["Email"].ToString();
                        item.ticketId = row["threadId"].ToString();

                        list.Add(item);
                    }
                    foreach (Tickets item in list)
                    {
                        DataTable test = new DataTable();

                        cmd.CommandText = "SELECT CASE WHEN EXISTS (\n"
                      + "    SELECT *\n"
                      + "    FROM Uploads\n"
                      + $"    WHERE Reference ='{item.reference}'\n"
                      + ")\n"
                      + "THEN CAST(1 AS BIT)\n"
                      + "ELSE CAST(0 AS BIT) END AS Attachments";

                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            test.Load(dr);
                        }

                        item.hasDocuments = Convert.ToBoolean(test.Rows[0][0].ToString());

                    }
                }
            }
            catch (Exception e)
             {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
                throw;
            }

            return list;
        }

        [HttpPost]
        [Route("UpdateTicket")]
        //[Authorize(Roles = "user")]
        public ValidationResultsModel UpdateTicket([FromBody] Tickets data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();

            try
            {
                using(SqlConnection cn = new SqlConnection(_ticketConnectionString))
                {
                    if(cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                    DataTable dt = new DataTable();

                    string sql = "";

                    cmd.Parameters.Add(new SqlParameter("@id", data.ticketId));
                    cmd.Parameters.Add(new SqlParameter("@user", data.changeby));
                    cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T"," ")));
                    string status = "";
                    if (data.statusChange)
                    {
                        switch (data.status)
                        {
                            case "1":
                                cmd.Parameters.Add(new SqlParameter("@status", data.status));
                                sql = $"UPDATE Ticket SET StatusCode =@status ,ChangeBy = @user,ChangeDate = @date\n" +
                                      $" WHERE TicketID =@id";
                                status = "Requested";
                                break;
                            case "2":
                                if(data.time == null)
                                {
                                    cmd.Parameters.Add(new SqlParameter("@status", data.status));
                                    sql = $"UPDATE Ticket SET StatusCode = @status,ChangeBy = @user,ChangeDate =@date ,EstimatedCompletionTime = @date ,isCompleted = '1' \n" +
                                          $"WHERE TicketID =@id";
                                }
                                if (data.time != null)
                                {
                                    cmd.Parameters.Add(new SqlParameter("@time", data.time));
                                    cmd.Parameters.Add(new SqlParameter("@status", data.status));
                                    sql = $"UPDATE Ticket SET StatusCode = @status,ChangeBy = @user,ChangeDate =@date ,EstimatedCompletionTime = @date,completedTime =@time ,isCompleted = '1' \n" +
                                          $"WHERE TicketID =@id";
                                }
                                status = "Completed";
                                break;
                            case "3":
                                cmd.Parameters.Add(new SqlParameter("@status", data.status));
                                sql = $"UPDATE Ticket SET StatusCode = @status,ChangeBy = @user,ChangeDate =@date \n" +
                                      $" WHERE TicketID =@id";
                                status = "Inprogress";
                                break;
                            case "4":
                                cmd.Parameters.Add(new SqlParameter("@status", data.status));
                                cmd.Parameters.Add(new SqlParameter("@reason", data.cancelReason));

                                sql = $"UPDATE Ticket SET StatusCode =@status ,ChangeBy = @user,ChangeDate = @date,cancelationDate = @date,CancellationReason= @reason,isCanceled='1' \n" +
                                      $"WHERE TicketID =@id";
                                status = "Canceled";
                                break;
                            default:
                                break;
                        }

                        cmd.CommandText = sql;
                        cmd.ExecuteNonQuery();
                    }

                    if (data.commentChange)
                    {
                        cmd.Parameters.Add(new SqlParameter("@comment", data.comment));
                        sql = $"UPDATE Ticket SET commentId =@comment,ChangeBy =@user,ChangeDate = @date\n" +
                              $" WHERE TicketID =@id";
                        cmd.CommandText = sql;
                        cmd.ExecuteNonQuery();
                    }


                    if (data.assignedChange)
                    {
                        cmd.Parameters.Add(new SqlParameter("@AssignedTo", data.assignedTo));
                        sql = $"UPDATE Ticket SET AssignedTo =@AssignedTo ,ChangeBy =@user ,ChangeDate = @date\n" +
                              $" WHERE TicketID =@id";
                        cmd.CommandText = sql;
                        cmd.ExecuteNonQuery();
                    }

                    if (data.categoryChange)
                    {
                        cmd.Parameters.Add(new SqlParameter("@cat", data.catid));
                        sql = $"UPDATE Ticket SET categoryId =@cat ,ChangeBy=@user,ChangeDate = @date\n" +
                              $" WHERE TicketID =@id";
                        cmd.CommandText = sql;
                        cmd.ExecuteNonQuery();
                    }


                    if (data.importanceChange)
                    {
                        cmd.Parameters.Add(new SqlParameter("@imp", data.importance));
                        sql = $"UPDATE Ticket SET importanceCode=@imp,ChangeBy=@user,ChangeDate=@date\n" +
                              $"WHERE TicketID =@id";
                        cmd.CommandText = sql;
                        cmd.ExecuteNonQuery();
                    }

                    if (data.statusChange || data.assignedChange)
                    {
                        cmd.CommandText = $"SELECT * FROM Ticket WHERE TicketID =@id";

                        dt.Load(cmd.ExecuteReader());


                        MailMessage msg = new MailMessage();

                        msg.From = new MailAddress("noreplys@pamc.co.za");


                        string subject = $"Ticket {dt.Rows[0][9].ToString()}";

                        msg.IsBodyHtml = false;

                        string to = dt.Rows[0][6].ToString();
                        string body = "";
                        if (data.statusChange)
                        {
                            body = $"Good day, \n\n Your tickets status changed to '{status}'.\n\n Regards \nPAMC Team";
                        }
                        if (data.assignedChange)
                        {
                            body = $"Good day, \n\n Your ticket was assigned to a new user: {data.assignedTo}.\n\n Regards \nPAMC Team";
                        }

                        if (data.statusChange && data.assignedChange)
                        {
                            body = $"Good day, \n\n Your tickets status changed to '{status}' and was assigned to a new user: {data.assignedTo}.\n\n Regards \nPAMC Team";
                        }

                        cmd.CommandText = "SELECT * FROM DRC..EDI_MAILSERVERS WHERE ROWID = '4'";
                        dt = new DataTable();
                        dt.Load(cmd.ExecuteReader());

                        string server = "";
                        string username = "";
                        string password = "";
                        string from = "";
                        string port = "";

                        foreach (DataRow row in dt.Rows)
                        {
                            server = row["SERVERADDRESS"].ToString();
                            password = row["PASSWORD"].ToString();
                            username = row["USERNAME"].ToString();
                            from = row["MAILFROM"].ToString();
                            port = row["OUTGOINGPORT"].ToString();

                        }


                        vr.valid = PAMC.EmailSender.Sender.SendEmailViaSMTP(server, username, password, from, subject, body, to, null);

                    }
                }

               // vr.valid = true;
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
                throw;
            }
            return vr;
        }

        [HttpGet]
        [Route("GetAllTickets")]
        //[Authorize(Roles = "user")]
        public List<Tickets> GetCanceledTickets()
        {
            List<Tickets> list = new List<Tickets>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_ticketConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }
                    SqlCommand cmd = new SqlCommand();
                    DataTable dt = new DataTable();

                    cmd.Connection = cn;

                    string date = DateTime.Now.AddMonths(-3).ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ");

                    string sql = "SELECT Ticket.CreateDate, \n"
           + "       Ticket.username, \n" +
           "         Ticket.surname,\n"
           + "       Ticket.TicketID, \n"
           + "       Ticket.threadId, \n"
           + "       Ticket.AssignedTo, \n"
           + "       Ticket.Username, \n"
           + "       TIcket.Email, \n"
           + "       Ticket.Subject, \n"
           + "       Ticket.Detail, \n"
           + "       Ticket.StatusCode, \n"
           + "       Ticket.Reference, \n"
           + "       tc.categoryDescription, \n"
           + "       LI.importanceDescription, \n"
           + "       Ticket.AcknowledgeDate, \n"
           + "       Ticket.completedTime," +
           "s.StatusDescription,\n" +
           "         Ticket.ChangeDate," +
           "         Ticket.ChangeBy,Ticket.CancellationReason," +
           "         c.clientName,Ticket.completedTime," +
           "c.clientId,Ticket.categoryId,Ticket.commentId \n"
           + "FROM Ticket\n" +
           "INNER JOIN Clients c ON c.ClientId = Ticket.clientId"
           + "     INNER JOIN TicketCategory tc ON tc.categoryId = Ticket.categoryId\n"
           + "     INNER JOIN TicketStatus s ON s.StatusCode = Ticket.StatusCode\n"
           + "     INNER JOIN LevelOfImportance LI ON LI.importanceCode = TIcket.importanceCode\n" +
           $"WHERE Ticket.ChangeDate > '{date}'";


                    cmd.CommandText = sql;

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }

                    foreach (DataRow row in dt.Rows)
                    {
                        Tickets item = new Tickets();
                        item.name = row["username"].ToString();
                        item.surname = row["surname"].ToString();
                        item.status = row["StatusCode"].ToString();
                        item.assignedTo = row["AssignedTo"].ToString();
                        item.ticketId = row["TicketID"].ToString();
                        item.threadId = row["threadId"].ToString();
                        item.aknowledge = row["AcknowledgeDate"].ToString();
                        item.user = row["Username"].ToString();
                        item.message = row["Detail"].ToString();
                        item.subject = row["Subject"].ToString();
                        item.reference = row["Reference"].ToString();
                        item.importance = row["importanceDescription"].ToString();
                        item.category = row["categoryDescription"].ToString();
                        item.createDate = row["CreateDate"].ToString();
                        item.completedTime = row["completedTime"].ToString();
                        item.email = row["Email"].ToString();
                        item.clientIdDesc = row["clientName"].ToString();
                        item.clientId = row["clientId"].ToString();
                        item.changeby = row["ChangeBy"].ToString();
                        item.changeDate = row["ChangeDate"].ToString();
                        item.comment = row["commentId"].ToString();
                        item.time = row["completedTime"].ToString();
                        item.cancelReason = row["CancellationReason"].ToString();
                        item.statDisc = row["StatusDescription"].ToString();

                        list.Add(item);
                    }

                    foreach (Tickets item in list)
                    {
                        DataTable test = new DataTable();

                        cmd.CommandText = "SELECT CASE WHEN EXISTS (\n"
                      + "    SELECT *\n"
                      + "    FROM Uploads\n"
                      + $"    WHERE Reference ='{item.reference}'\n"
                      + ")\n"
                      + "THEN CAST(1 AS BIT)\n"
                      + "ELSE CAST(0 AS BIT) END AS Attachments";

                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            test.Load(dr);
                        }

                        item.hasDocuments = Convert.ToBoolean(test.Rows[0][0].ToString());

                    }
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
                throw;
            }

            return list;
        }

        [HttpGet]
        [Route("GetUserList")]
        //[Authorize(Roles = "user")]
        public List<userList> GetUserList()
        {
            List<userList> list = new List<userList>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_ticketConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }
                    SqlCommand cmd = new SqlCommand();
                    DataTable dt = new DataTable();

                    cmd.Connection = cn;

                    string date = DateTime.Now.AddMonths(-12).ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ");

                    string sql = "SELECT * FROM USERS WHERE UserType = 2";


                    cmd.CommandText = sql;

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }

                    foreach (DataRow row in dt.Rows)
                    {
                        userList item = new userList();
                        item.name = row["Name"].ToString();
                        item.surname = row["Surname"].ToString();
                        item.username = row["Username"].ToString();
                        item.clientId = row["clientId"].ToString();
                        item.email = row["Email"].ToString();
                        item.usetype = row["UserType"].ToString();

                        list.Add(item);
                    }
                     
                    
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
                throw;
            }

            return list;
        }

        [HttpGet]
        [Route("GetCommentsList")]
        //[Authorize(Roles = "user")]
        public List<Comments> GetCommentsList()
        {
            List<Comments> list = new List<Comments>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_ticketConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }
                    SqlCommand cmd = new SqlCommand();
                    DataTable dt = new DataTable();

                    cmd.Connection = cn;

                    string date = DateTime.Now.AddMonths(-12).ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ");

                    string sql = "SELECT * FROM AdminComment ";


                    cmd.CommandText = sql;

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }

                    foreach (DataRow row in dt.Rows)
                    {
                        Comments item = new Comments();
                        item.id = row["commentId"].ToString();
                        item.comment = row["commentDescription"].ToString();

                        list.Add(item);
                    }


                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
                throw;
            }

            return list;
        }

        [HttpPost]
        [Route("GetCategories")]
        //[Authorize(Roles = "user")]
        public List<Categories> GetCategories([FromBody] Tickets data)
        {
            List<Categories> list = new List<Categories>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_ticketConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }
                    SqlCommand cmd = new SqlCommand();
                    DataTable dt = new DataTable();

                    cmd.Connection = cn;

                    string date = DateTime.Now.AddMonths(-12).ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ");

                    cmd.Parameters.Add(new SqlParameter("@id", data.clientId));

                    string sql = "SELECT c.categoryId,t.categoryDescription FROM ClientCategory c\n" +
                        "INNER JOIN TicketCategory t  ON t.categoryId = c.categoryId \n" +
                        "WHERE clientId = @id";


                    cmd.CommandText = sql;

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }

                    foreach (DataRow row in dt.Rows)
                    {
                        Categories item = new Categories();
                        item.id = row["categoryId"].ToString();
                        item.desc = row["categoryDescription"].ToString();

                        list.Add(item);
                    }


                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
                throw;
            }

            return list;
        }

        [HttpGet]
        [Route("GetImportanceCodes")]
        //[Authorize(Roles = "user")]
        public List<Importance> GetImportanceCodes()
        {
            List<Importance> list = new List<Importance>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_ticketConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }
                    SqlCommand cmd = new SqlCommand();
                    DataTable dt = new DataTable();

                    cmd.Connection = cn;

                    string date = DateTime.Now.AddMonths(-12).ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ");

                    string sql = "SELECT * FROM LevelOfImportance ";


                    cmd.CommandText = sql;

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }

                    foreach (DataRow row in dt.Rows)
                    {
                        Importance item = new Importance();
                        item.id = row["importanceCode"].ToString();
                        item.desc = row["importanceDescription"].ToString();

                        list.Add(item);
                    }


                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
                throw;
            }

            return list;
        }

        [HttpGet]
        [Route("GetStatus")]
        //[Authorize(Roles = "user")]
        public List<Status> GetStatus()
        {
            List<Status> list = new List<Status>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_ticketConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }
                    SqlCommand cmd = new SqlCommand();
                    DataTable dt = new DataTable();

                    cmd.Connection = cn;

                    string date = DateTime.Now.AddMonths(-12).ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ");

                    string sql = "SELECT * FROM TicketStatus ";


                    cmd.CommandText = sql;

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }

                    foreach (DataRow row in dt.Rows)
                    {
                        Status item = new Status();
                        item.code = row["StatusCode"].ToString();
                        item.desc = row["StatusDescription"].ToString();

                        list.Add(item);
                    }


                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
                throw;
            }

            return list;
        }

        [HttpGet]
        [Route("GetDurations")]
        //[Authorize(Roles = "user")]
        public List<Durations> GetDurations()
        {
            List<Durations> list = new List<Durations>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_ticketConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }
                    SqlCommand cmd = new SqlCommand();
                    DataTable dt = new DataTable();

                    cmd.Connection = cn;

                    string date = DateTime.Now.AddMonths(-12).ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ");

                    string sql = "SELECT * FROM Duration ";


                    cmd.CommandText = sql;

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }

                    foreach (DataRow row in dt.Rows)
                    {
                        Durations item = new Durations();
                        item.timeSpent = row["timeSpent"].ToString();
                        item.minutes = row["actualMinutes"].ToString();

                        list.Add(item);
                    }


                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
                throw;
            }

            return list;
        }

        [HttpPost]
        [Route("GetInProgressTickets")]
        //[Authorize(Roles = "user")]
        public List<Tickets> GetInProgressTickets([FromBody] login data)
        {
            List<Tickets> list = new List<Tickets>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_ticketConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }
                    SqlCommand cmd = new SqlCommand();
                    DataTable dt = new DataTable();

                    cmd.Parameters.Add(new SqlParameter("@username", data.username));
                    cmd.Parameters.Add(new SqlParameter("@status", "3"));

                    cmd.Connection = cn;

                    string sql = "SELECT Ticket.CreateDate, \n"
           + "       Ticket.Username, \n"
           + "       TIcket.Email, \n"
           + "       Ticket.Subject, \n"
           + "       Ticket.Detail, \n"
           + "       Ticket.Reference, \n"
           + "       tc.categoryDescription, \n"
           + "       LI.importanceDescription, \n"
           + "       Ticket.AcknowledgeDate, \n"
           + "       Ticket.completedTime\n"
            + "       Ticket.threadId\n"
           + "FROM Ticket\n"
           + "     INNER JOIN TicketCategory tc ON tc.categoryId = Ticket.categoryId\n"
           + "     INNER JOIN LevelOfImportance LI ON LI.importanceCode = TIcket.importanceCode\n"
           + "WHERE Ticket.AssignedTo = @username AND Ticket.StatusCode = @status";

                    cmd.CommandText = sql;

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }

                    foreach (DataRow row in dt.Rows)
                    {
                        Tickets item = new Tickets();

                        item.aknowledge = row["AcknowledgeDate"].ToString();
                        item.user = row["Username"].ToString();
                        item.message = row["Detail"].ToString();
                        item.subject = row["Subject"].ToString();
                        item.reference = row["Reference"].ToString();
                        item.importance = row["importanceDescription"].ToString();
                        item.category = row["categoryDescription"].ToString();
                        item.createDate = row["CreateDate"].ToString();
                        item.completedTime = row["completedTime"].ToString();
                        item.email = row["Email"].ToString();
                        item.ticketId = row["threadId"].ToString();
                        list.Add(item);
                    }



                    foreach (Tickets item in list)
                    {
                        DataTable test = new DataTable();
                        
                        cmd.CommandText = "SELECT CASE WHEN EXISTS (\n"
                      + "    SELECT *\n"
                      + "    FROM Uploads\n"
                      + $"    WHERE ticketThreadId ='{item.ticketId}'\n"
                      + ")\n"
                      + "THEN CAST(1 AS BIT)\n"
                      + "ELSE CAST(0 AS BIT) END AS Attachments";

                        using(SqlDataReader dr = cmd.ExecuteReader())
                        {
                            test.Load(dr);
                        }

                        item.hasDocuments = Convert.ToBoolean(test.Rows[0][0].ToString());
                      
                    }
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }

            return list;
        }

        [HttpPost]
        [Route("GetNewTickets")]
        //[Authorize(Roles = "user")]
        public List<Tickets> GetNewTickets([FromBody] login data)
        {
            List<Tickets> list = new List<Tickets>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_ticketConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }
                    SqlCommand cmd = new SqlCommand();
                    DataTable dt = new DataTable();

                    cmd.Parameters.Add(new SqlParameter("@username", data.username));
                    cmd.Parameters.Add(new SqlParameter("@status", "1"));

                    cmd.Connection = cn;

                    string sql = "SELECT Ticket.CreateDate, \n"
           + "       Ticket.Username, \n"
           + "       TIcket.Email, \n"
           + "       Ticket.Subject, \n"
           + "       Ticket.Detail, \n"
           + "       Ticket.Reference, \n"
           + "       tc.categoryDescription, \n"
           + "       LI.importanceDescription, \n"
           + "       Ticket.AcknowledgeDate, \n"
           + "       Ticket.completedTime\n"
            + "       Ticket.threadId\n"
           + "FROM Ticket\n"
           + "     INNER JOIN TicketCategory tc ON tc.categoryId = Ticket.categoryId\n"
           + "     INNER JOIN LevelOfImportance LI ON LI.importanceCode = TIcket.importanceCode\n"
           + "WHERE Ticket.AssignedTo = @username AND Ticket.StatusCode = @status";

                    cmd.CommandText = sql;

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }

                    foreach (DataRow row in dt.Rows)
                    {
                        Tickets item = new Tickets();

                        item.aknowledge = row["AcknowledgeDate"].ToString();
                        item.user = row["Username"].ToString();
                        item.message = row["Detail"].ToString();
                        item.subject = row["Subject"].ToString();
                        item.reference = row["Reference"].ToString();
                        item.importance = row["importanceDescription"].ToString();
                        item.category = row["categoryDescription"].ToString();
                        item.createDate = row["CreateDate"].ToString();
                        item.completedTime = row["completedTime"].ToString();
                        item.email = row["Email"].ToString();
                        item.ticketId = row["threadId"].ToString();
                        list.Add(item);
                    }



                    foreach (Tickets item in list)
                    {
                        DataTable test = new DataTable();

                        cmd.CommandText = "SELECT CASE WHEN EXISTS (\n"
                      + "    SELECT *\n"
                      + "    FROM Uploads\n"
                      + $"    WHERE ticketThreadId ='{item.ticketId}'\n"
                      + ")\n"
                      + "THEN CAST(1 AS BIT)\n"
                      + "ELSE CAST(0 AS BIT) END AS Attachments";

                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            test.Load(dr);
                        }

                        item.hasDocuments = Convert.ToBoolean(test.Rows[0][0].ToString());

                    }
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }

            return list;
        }

        [HttpPost]
        [Route("DownloadSelectedDocs")]
        public List<testing1.models.Attachment> DownloadSelectedDocs([FromBody] Tickets data)
        {
            string rootFolder = _env.WebRootPath;
            string contentRoot = _env.ContentRootPath;
            DirectoryInfo content = new DirectoryInfo(contentRoot + $"\\ClientApp\\src\\assets\\Docs");

            List<testing1.models.Attachment> files = new List<testing1.models.Attachment>();
            try
            {
                using(SqlConnection cn = new SqlConnection(_ticketConnectionString))
                {
                    if(cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    DataTable dt = new DataTable();

                    cmd.Connection = cn;
                    cmd.Parameters.Add(new SqlParameter("@ref", data.reference));

                    cmd.CommandText = $"SELECT * FROM Uploads WHERE Reference = @ref";

                    using(SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }

                    if (content.Exists == false)
                    {
                        content = new DirectoryInfo(contentRoot + $"\\ClientApp\\dist\\assets\\Docs");
                    }

                    foreach(DataRow row in dt.Rows)
                    {
                        testing1.models.Attachment a = new testing1.models.Attachment();
                        var filebytes = (byte[])row["FileContent"];

                        //c = ;
                        a.name = row["FileName"].ToString();
                        
                        using(MemoryStream ms = new MemoryStream(filebytes))
                        {
                            System.IO.File.WriteAllBytes(content.FullName+"\\"+a.name, filebytes);
                        }
                        files.Add(a);
                        
                    }


                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
                throw;
            }

            return files;
        }
    
        [HttpGet]
        [Route("GetQoutes")]
        //[Authorize(Roles = "user")]
        public List<Qoutes> GetQoutes()
        {
            List<Qoutes> list = new List<Qoutes>();
            try
            {
                string rootFolder = _env.WebRootPath;
                DirectoryInfo content = new DirectoryInfo(rootFolder + $"\\tmp");
                FileInfo f = new FileInfo(content.FullName + "\\qoutes.txt");


                var lines = System.IO.File.ReadAllLines(f.FullName);

                for (int i = 0; i < lines.Length; i++)
                {
                    string[] line = new string[2];
                    line = lines[i].Split(',');

                    Qoutes q = new Qoutes();
                    q.desc = line[0].Replace(";",",");
                    q.author = line[1];

                    list.Add(q);
                }

                
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
                throw;
            }
            return list;
        }
    
    
        [HttpPost]
        [Route("GetMessages")]
        //[Authorize(Roles ="user")]
        public List<Tickets> GetMessages([FromBody] Tickets ticket)
        {
            List<Tickets> tc = new List<Tickets>();

            try
            {
                using(SqlConnection cn = new SqlConnection(_ticketConnectionString))
                {
                    if(cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    DataTable dt = new DataTable();

                    cmd.Connection = cn;

                    cmd.Parameters.Add(new SqlParameter("@ref", ticket.reference));

                    cmd.CommandText = $"SELECT * FROM {TicketDatabase}.dbo.Ticketthread WHERE Reference = @ref";

                    using(SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }

                    foreach (DataRow row in dt.Rows)
                    {
                        Tickets a = new Tickets();

                        a.assignedTo = row["createBy"].ToString();
                        a.message = row["Detail"].ToString();
                        a.createDate = row["CreateDate"].ToString();

                        tc.Add(a);
                    }

                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
                throw;
            }

            return tc;
        }

        [HttpPost]
        [Route("SendReply")]
        //[Authorize(Roles = "user")]
        public List<Tickets> SendReply([FromBody] Tickets ticket)
        {
            List<Tickets> tc = new List<Tickets>();

            try
            {
                using (SqlConnection cn = new SqlConnection(_ticketConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    DataTable dt = new DataTable();

                    cmd.Connection = cn;


                    cmd.Parameters.Add(new SqlParameter("@id", ticket.threadId));
                    cmd.Parameters.Add(new SqlParameter("@ref", ticket.reference));
                    cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T"," ")));
                    cmd.Parameters.Add(new SqlParameter("@user", ticket.user));
                    cmd.Parameters.Add(new SqlParameter("@msg", ticket.message));

                    cmd.CommandText = $"INSERT INTO {TicketDatabase}.dbo.Ticketthread (Reference,CreateDate,createBy,Detail,ChangeDate,ChangeBy)\n" +
                        $"OUTPUT INSERTED.ticketThreadId\n" +
                        $"VALUES (@ref,@date,@user,@msg,@date,@user)";
                    //cmd.ExecuteNonQuery();
                    _threadid = cmd.ExecuteScalar().ToString();


                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
                throw;
            }

            return tc;
        }

        [HttpPost, DisableRequestSizeLimit]
        [Route("UploadFiles")]
        //[Authorize(Roles = "user")]
        public ValidationResultsModel UploadFiles()
        {
            ValidationResultsModel tc = new ValidationResultsModel();

            try
            {
                using (SqlConnection cn = new SqlConnection(_ticketConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    DataTable dt = new DataTable();

                    cmd.Connection = cn;

 
                    cmd.Parameters.Add(new SqlParameter("@id", _threadid));

                    string sql = $"SELECT t.Reference,t.Detail,u.Email FROM {TicketDatabase}.dbo.Ticketthread t \n" +
                        $"INNER JOIN  {TicketDatabase}.dbo.Users u ON u.Username = t.createBy \n" +
                        $"WHERE t.ticketThreadId = @id";
                    cmd.CommandText = sql;
                    dt.Load(cmd.ExecuteReader());

                    Tickets t = new Tickets();          

                    foreach (DataRow row in dt.Rows)
                    {
                        t.reference = row["Reference"].ToString();
                        t.message = row["Detail"].ToString();
                        t.user = row["Email"].ToString();
                        
                        
                    }

                    string date = DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ");


                   
                    var file = Request.Form.Files;
                    foreach (var item in file)
                    {
                        using (var ms = new MemoryStream())
                        {
                            item.CopyTo(ms);
                            byte[] filebytes = ms.ToArray();

                                if (cn.State != ConnectionState.Open)
                                {
                                    cn.Open();
                                }

                            cmd.Parameters.Clear();

                            cmd.Parameters.Add("@array", SqlDbType.VarBinary, filebytes.Length).Value = filebytes;

                            cmd.CommandText = $"INSERT INTO {TicketDatabase}.dbo.Uploads (ticketThreadId,Reference,FileName,FileContent" +
                                    $") VALUES ('{_threadid}','{ t.reference}','{item.FileName}',@array)";

                                cmd.ExecuteNonQuery();
                                
                            
                        }
                    }
                    DataTable tbl = new DataTable();
                    cmd.CommandText = $"SELECT Email,Subject FROM {TicketDatabase}.dbo.Ticket WHERE reference = '{t.reference}'";

                    tbl.Load(cmd.ExecuteReader());

                    t.email = tbl.Rows[0][0].ToString();
                    t.subject = tbl.Rows[0][1].ToString();

                    MailMessage msg = new MailMessage();

                    
                    
                    if(t.email == t.user)
                    {
                         tbl = new DataTable();
                        cmd.CommandText = $"SELECT u.Email FROM {TicketDatabase}.dbo.Ticket t \n" +
                            $"INNER JOIN {TicketDatabase}.dbo.Users u ON u.Username = t.AssignedTo\n" +
                            $" WHERE reference = '{t.reference}'";

                        tbl.Load(cmd.ExecuteReader());

                        t.email = tbl.Rows[0][0].ToString();
                       
                    }
                    else
                    {
                       
                    }

                   
                    msg.Subject = t.reference;

                    msg.IsBodyHtml = false;
                    DirectoryInfo d = new DirectoryInfo($"{_env.WebRootPath}\\files\\");

                    int i = d.GetFiles().Length + 1;

                    DirectoryInfo dd = new DirectoryInfo($"{_env.WebRootPath}\\files\\{DateTime.Now.ToString("yyyyMMdd")}_{i}");

                    dd.Create();
                    //foreach (FileInfo item in d.GetFiles())
                    //{
                    //    if (item.Exists)
                    //    {
                    //        try
                    //        {
                    //            item.Delete();
                    //        }
                    //        catch (Exception)
                    //        {

                              
                    //        }
                    //    }
                        
                    //}
                    
                     List < FileInfo > ff = new List<FileInfo>();
                    foreach (var item in file)
                    {
                        using (var ms = new MemoryStream())
                        {
                            item.CopyTo(ms);
                            byte[] filebytes = ms.ToArray();
                            FileStream fs = new FileStream(_env.WebRootPath + $"\\files\\{DateTime.Now.ToString("yyyyMMdd")}_{i}\\{item.FileName}", FileMode.Create);
                            MemoryStream m = new MemoryStream(filebytes);
                            m.WriteTo(fs);
                            fs.Close();
                            m.Close();
                            fs.Dispose();
                            m.Dispose();
                            FileInfo files = new FileInfo(_env.WebRootPath + $"\\files\\{DateTime.Now.ToString("yyyyMMdd")}_{i}\\{item.FileName}");
                            if (files.Exists)
                            {
                                ff.Add(files);
                            }

                        }
                    }

                                       string body = "Good day, \n\n You have a reply on the folowing ticket:\n\n " +
                        $"Reference: {t.reference} \n" +
                        $"Subject: {t.subject}\n" +
                        $"message: {t.message} \n" +
                        $"Please find {msg.Attachments.Count} attachment for your attention. \n\n" +
                        $"Kind Regards\n" +
                        $"PAMC Team";

                    cmd.CommandText = "SELECT * FROM DRC..EDI_MAILSERVERS WHERE ROWID = '4'";
                    dt = new DataTable();
                    dt.Load(cmd.ExecuteReader());

                    string server = "";
                    string username = "";
                    string password = "";
                    string from = "";
                    string port = "";

                    foreach (DataRow row in dt.Rows)
                    {
                        server = row["SERVERADDRESS"].ToString();
                        password = row["PASSWORD"].ToString();
                        username = row["USERNAME"].ToString();
                        from = row["MAILFROM"].ToString();
                        port = row["OUTGOINGPORT"].ToString();

                    }


                    tc.valid = PAMC.EmailSender.Sender.SendEmailViaSMTP(server, username, password, from, t.reference, body, t.email, ff);

                    tc.valid = true;
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
                throw;
            }

            return tc;
        }

        [HttpPost]
        [Route("SubmitNewTicket")]
        //[Authorize(Roles = "user")]
        public ValidationResultsModel SubmitNewTicket([FromBody] Tickets data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            try
            {
                using(SqlConnection cn = new SqlConnection(_ticketConnectionString))
                {
                    if(cn.State != ConnectionState.Open)
                    {
                        cn.Open();

                    }

                    data.message = data.message.Replace("'", "");

                    string names = "";
                    string reference = "";
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    DataTable dt = new DataTable();
                    DataTable tbl = new DataTable();

                    cmd.Parameters.Add(new SqlParameter("@catid", data.catid));
                    cmd.Parameters.Add(new SqlParameter("@clientId", data.clientId));
                    string sql = "";

                    sql = $"SELECT * FROM ClientCategory WHERE categoryId = @catID  AND clientId = @clientId";

                    cmd.CommandText = sql;
                    dt.Load(cmd.ExecuteReader());

                    if(dt.Rows.Count > 1)
                    {
                        sql = $"SELECT AssignedTo,count(*) as 'TICKETCOUNT' " +
                            $"FROM Ticket "+
                            "WHERE clientID = @clientId and statuscode = '1' " +
                            "group by AssignedTo " +
                            "order by TICKETCOUNT asc ";
                        cmd.CommandText = sql;
                        dt = new DataTable();
                        dt.Load(cmd.ExecuteReader());
                        names = dt.Rows[0]["AssignedTo"].ToString();
                    }
                    else
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            names = row["userName"].ToString();
                        }
                    }


                    dt = new DataTable();


                    sql = $"SELECT * FROM Clients WHERE clientId = @clientId";

                    cmd.CommandText = sql;
                    dt.Load(cmd.ExecuteReader());

                    reference = dt.Rows[0][2].ToString();

                    dt = new DataTable();

                    cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/dd") + " 00:00:00"));

                    sql = $"SELECT COUNT(*) FROM Ticket WHERE clientId = @clientId AND CreateDate > @date";

                    cmd.CommandText = sql;
                    dt.Load(cmd.ExecuteReader());

                    reference = DateTime.Now.ToString("yyyyMMdd") + reference + (Convert.ToInt32(dt.Rows[0][0].ToString()) + 1).ToString();

                    data.reference = reference;

                    dt = new DataTable();

                    string assignTo = "";
                    int count = 0;
                    assignTo = names;

                    sql = $"INSERT INTO Ticketthread (Reference,CreateDate,createBy,Detail,ChangeDate,ChangeBy) \n" +
                        $"OUTPUT INSERTED.ticketThreadId \n" +
                        $"VALUES('{reference}','{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T"," ")}'," +
                        $"'{data.user}','{data.message}','{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}','{data.user}')";
                    dt = new DataTable();
                    cmd.CommandText = sql;
                    data.threadId = cmd.ExecuteScalar().ToString();
                    _threadid = data.threadId;

                    sql = $"INSERT INTO Ticket (threadId," +
                        $"clientID," +
                        $"Username," +
                        $"Name," +
                        $"Surname," +
                        $"Email," +
                        $"Subject," +
                        $"Detail," +
                        $"Reference," +
                        $"StatusCode," +
                        $"categoryId," +
                        $"importanceCode," +
                        $"CreateDate," +
                        $"ChangeDate," +
                        $"AssignedTo," +
                        $"ChangeBy) \n" +
                        $"OUTPUT INSERTED.TicketID \n" +
                        $"VALUES('{data.threadId}'," +
                        $"'{data.clientId}'," +
                        $"'{data.user}'," +
                        $"'{data.name}'," +
                        $"'{data.surname}'," +
                        $"'{data.email}'," +
                        $"'{data.subject}'," +
                        $"'{data.message}'," +
                        $"'{data.reference}'," +
                        $"'1'," +
                        $"'{data.catid}'," +
                        $"'{data.importance}'," +
                        $"'{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}'," +
                        $"'{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}'," +
                        $"'{assignTo}'," +
                        $"'{data.user}')";
                    cmd.CommandText = sql;
                    _newid = cmd.ExecuteScalar().ToString();




                   


                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
                throw;
            }
            return vr;
        }

        [HttpPost]
        [Route("UploadNewTicketFiles")]
        //[Authorize(Roles = "user")]
        public ValidationResultsModel UploadNewTicketFiles()
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            try
            {
                using (SqlConnection cn = new SqlConnection(_ticketConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();

                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    DataTable dt = new DataTable();

                    string reference = "";
                    string assigntoEmail = "";
                    string createemail = "";
                    string subject = "";
                    string message = "";

                    cmd.CommandText = $"SELECT t.Reference,u.Email,t.Detail,t.Subject,t.Email FROM Ticket t \n" +
                        $"INNER JOIN Users u ON u.username = t.AssignedTo \n" +
                        $"WHERE t.TicketID = '{_newid}'";

                    dt.Load(cmd.ExecuteReader());

                    reference = dt.Rows[0][0].ToString();
                    assigntoEmail = dt.Rows[0][1].ToString();
                    subject = dt.Rows[0][3].ToString();
                    message = dt.Rows[0][2].ToString();
                    createemail = dt.Rows[0][4].ToString();

                    var file = Request.Form.Files;
                    foreach (var item in file)
                    {
                        using (var ms = new MemoryStream())
                        {
                            item.CopyTo(ms);
                            byte[] filebytes = ms.ToArray();

                            if (cn.State != ConnectionState.Open)
                            {
                                cn.Open();
                            }

                            cmd.Parameters.Clear();
                            cmd.Parameters.Add("@array", SqlDbType.VarBinary, filebytes.Length).Value = filebytes;

                            cmd.CommandText = $"INSERT INTO {TicketDatabase}.dbo.Uploads (ticketThreadId,Reference,FileName,FileContent" +
                                    $") VALUES ('{_threadid}','{ reference}','{item.FileName}',@array)";

                            cmd.ExecuteNonQuery();
                            //cn.Close();

                        }
                    }



                    DirectoryInfo d = new DirectoryInfo($"{_env.WebRootPath}\\files\\");
                    int i = d.GetFiles().Length +1;

                    DirectoryInfo dd = new DirectoryInfo($"{_env.WebRootPath}\\files\\{DateTime.Now.ToString("yyyyMMdd")}_{i}");

                    dd.Create();

                    List<FileInfo> ff = new List<FileInfo>();
                    foreach (var item in file)
                    {
                        using (var ms = new MemoryStream())
                        {
                            item.CopyTo(ms);
                            byte[] filebytes = ms.ToArray();

                            MemoryStream m = new MemoryStream(filebytes);
                            FileStream fs = new FileStream(_env.WebRootPath + $"\\files\\{DateTime.Now.ToString("yyyyMMdd")}_{i}\\{item.FileName}",FileMode.Create);
                            m.WriteTo(fs);
                            fs.Close();
                            m.Close();
                            FileInfo files = new FileInfo(_env.WebRootPath + $"\\files\\{DateTime.Now.ToString("yyyyMMdd")}_{i}\\{item.FileName}");
                            if (files.Exists)
                            {
                                ff.Add(files);
                            }

                        }
                    }



                    cmd.CommandText = "SELECT * FROM DRC..EDI_MAILSERVERS WHERE ROWID = '4'";
                    dt = new DataTable();
                    dt.Load(cmd.ExecuteReader());

                    string server = "";
                    string username = "";
                    string password = "";
                    string from = "";
                    string port = "";

                    foreach (DataRow row in dt.Rows)
                    {
                        server = row["SERVERADDRESS"].ToString();
                        password = row["PASSWORD"].ToString();
                        username = row["USERNAME"].ToString();
                        from = row["MAILFROM"].ToString();
                        port = row["OUTGOINGPORT"].ToString();

                    }

                    string body = "Good day, \n\n You have a new ticket:\n\n " +
    $"Reference: {reference} \n" +
    $"Subject: {subject}\n" +
    $"message: {message} \n" +
    $"Please find {ff.Count} attachment for your attention. \n\n" +
    $"Kind Regards\n" +
    $"PAMC Team";

                    vr.valid = PAMC.EmailSender.Sender.SendEmailViaSMTP(server, username, password, from, $"Ticket  {reference}", body, $"{assigntoEmail};{createemail}", ff);

                }
            }
            catch (Exception e)
            {

                var msg = e.Message;
                LogError(e.Message, e.StackTrace);
                throw;
            }
            return vr;
        }


        [HttpPost]
        public void LogError(string error, string stacktrace)
        {
            string date = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            string fileName = "";
            string controler = "SECURITY";
            var root = _env.WebRootPath;

            fileName = $"E_{controler}_{date}.txt";

            FileInfo fi = new FileInfo($"{root}\\Error_Log\\{fileName}");
            //  fi.Create();

            try
            {
                using (StreamWriter fs = new StreamWriter(fi.FullName))
                {
                    fs.Write("ERROR: \n\n" + error + "\n\n STACK TRACE: \n\n" + stacktrace);
                }
            }
            catch (Exception)
            {


            }


        }
    }
}